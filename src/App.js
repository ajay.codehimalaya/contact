import AddContact from './Components/AddContact';
import './App.css';
import {useState,UseEffect} from 'react'


function App() {

  //Name Feild Data
  const [firstName,setFirstName]=useState("")
  const [middleName,setMiddleName]=useState("")
  const [lastName,setLastName]=useState("")

  //Email Detail

  const [email,setEmail]=useState([{email:""}])

  const emailHandler= (e) => {
  
    setEmail([...email,{email:""}])
  }
  const deleteEmailHandler= (index) => {
  
    const values=[...email]
    values.splice(index,1);
    setEmail(values) 
  
  }
  

//Phone number detail

const [phone,setPhone]=useState([{phone:""}]);

const phoneHandler= (e) => {
  e.preventDefault()

  setPhone([...phone,{phone:""}])
}


const deletePhoneHandler= (index) => {
  
  const values=[...phone]
  values.splice(index,1);
  setPhone(values) 

}



  return (
    <div className="App">
      <div className="contact-wrapper">
      <AddContact 
      setFirstName={setFirstName}
      setMiddleName={setMiddleName}
      setLastName={setLastName}
      setEmail={setEmail}
      setPhone={setPhone}
      email={email}
      phone={phone}
      emailHandler={emailHandler}
      phoneHandler={phoneHandler}
      deleteEmailHandler={deleteEmailHandler}
      deletePhoneHandler={deletePhoneHandler}/>
      </div>
    </div>
  );
}

export default App;
